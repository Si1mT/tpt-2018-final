module.exports = function capitalize(name) {
    if (typeof name !== 'string') {
      throw new Error('bad input');
    }
    return name[0].toUpperCase() + name.slice(1);
  };
  
