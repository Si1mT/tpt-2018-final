const users = require('../../src/user');

it('match snapshots', () => {
  expect(users(1)).toMatchSnapshot();
  expect(users(56)).toMatchSnapshot();
  expect(users(1345)).toMatchSnapshot();
});
