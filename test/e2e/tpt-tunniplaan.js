var config = require('../../nightwatch.conf.js');

module.exports = {
    'tptlive': function(browser) {
        browser
            .resizeWindow(1280,800)
            .url('https://www.tptlive.ee/')
            .pause(1000)
            .saveScreenshot(config.imgpath(browser) + 'frontpage.png')
            .click('li[id="menu-item-1313"]')
            .saveScreenshot(config.imgpath(browser) + 'tunniplaanid.png')
            .click('a[href="https://tpt.siseveeb.ee/veebivormid/tunniplaan/tunniplaan?oppegrupp=226&nadal=03.12.2018"]')
            .saveScreenshot(config.imgpath(browser) + 'TA-17E tunniplaan.png')
            .pause(500)
    }
};
