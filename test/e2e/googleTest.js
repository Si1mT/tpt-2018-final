var config = require('../../nightwatch.conf.js');

module.exports = {
    'visual google': function(browser) {
        browser
            .url('https://www.google.ee/')
            .pause(500)
            .waitForElementVisible('body')
            .pause(1000)
            .setValue('input[class="gLFyf gsfi"]', ['tallinn', browser.Keys.ENTER])
            .pause(500)
            .assert.containsText('body','tallinn')
            .pause(500)
            .saveScreenshot(config.imgpath(browser) + 'tallinn.png')
            .pause(500)
            .click('h3[class="LC20lb"]')
            .pause(500)
            .saveScreenshot(config.imgpath(browser) + 'firstResult.png')
    }
};
